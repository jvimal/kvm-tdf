
ccflags-y += -Ivirt/kvm -Iarch/x86/kvm

CFLAGS_x86.o := -I.
CFLAGS_svm.o := -I.
CFLAGS_vmx.o := -I.

kvm-y			+= virt/kvm/kvm_main.o \
					virt/kvm/ioapic.o \
                    virt/kvm/coalesced_mmio.o \
                    virt/kvm/irq_comm.o \
                    virt/kvm/eventfd.o \
                    virt/kvm/assigned-dev.o
kvm-y	+= virt/kvm/iommu.o
kvm-y	+= virt/kvm/async_pf.o

kvm-y			+= x86.o mmu.o emulate.o i8259.o irq.o lapic.o \
			   i8254.o timer.o
kvm-intel-y		+= vmx.o
kvm-amd-y		+= svm.o

obj-m	+= kvm.o
obj-m	+= kvm-intel.o
obj-m	+= kvm-amd.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=`pwd`

